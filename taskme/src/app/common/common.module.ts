import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { NavbarComponent } from './component/navbar/navbar.component';
import { CalendarComponent } from './component/calendar/calendar.component';
import { CreateMeetingComponent } from './component/create-meeting/create-meeting.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { MeetingComponent } from './component/meeting/meeting.component';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';
import { IndexComponent } from './component/index/index.component';
import { SignupComponent } from './component/signup/signup.component';
import { LoginComponent } from './component/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { PagenotfoundComponent } from './component/pagenotfound/pagenotfound.component';
import { ApplicationComponent } from './component/application/application.component';
import { SprintlistComponent } from './component/sprintlist/sprintlist.component';
import { NotificationComponent } from './component/notification/notification.component';
import { SprintboardComponent } from './component/sprintboard/sprintboard.component';
import { TaskdetailComponent } from './component/taskdetail/taskdetail.component';
import { SettingComponent } from './component/setting/setting.component';
import { ContactusComponent } from './component/contactus/contactus.component';
import { ForgetpasswordComponent } from './component/forgetpassword/forgetpassword.component';


const routes: Routes = [
  { path: 'index', component: IndexComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'contactus', component: ContactusComponent},
  { path: 'forgetpassword', component: ForgetpasswordComponent},
  { path: 'app', component: ApplicationComponent,children: [
    { path: 'meeting', component: MeetingComponent},
    { path: 'sprintlist', component: SprintlistComponent},
    { path: 'sprintboard', component: SprintboardComponent},
    { path: 'taskdetail', component: TaskdetailComponent},
    { path: 'notification', component: NotificationComponent},
    { path: 'setting', component: SettingComponent},
  ]}
];
@NgModule({
  declarations: [NavbarComponent, CalendarComponent, CreateMeetingComponent, MeetingComponent, HeaderComponent, FooterComponent, IndexComponent, SignupComponent, LoginComponent, PagenotfoundComponent, ApplicationComponent, SprintlistComponent, NotificationComponent, SprintboardComponent, TaskdetailComponent, SettingComponent, ContactusComponent, ForgetpasswordComponent],
  imports: [
    CommonModule,
    FullCalendarModule,
    BrowserAnimationsModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    RouterModule
  ],
  providers: [DatePipe],
})
export class commonModule { }

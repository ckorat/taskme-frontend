import { Component, OnInit } from '@angular/core';
import { CreateMeetingComponent } from '../create-meeting/create-meeting.component';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.css']
})
export class MeetingComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }
  openDailoge(){
    this.dialog.open(CreateMeetingComponent);
}
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PopupService } from '../../services/popup/popup.service';
import { UserModel } from '../../model/user/user-model';
import { UserService } from '../../services/user/user.service';
import { ForgotPasswordModel } from '../../model/forgetpassword/forgetpassword-model';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {
  forgetPasswordForm: FormGroup;
  submitted = false;
  response: any;
  userData:UserModel;
  key='password123';
  
  constructor(private formBuilder: FormBuilder,private popupService:PopupService,private userService:UserService) { }

  ngOnInit() {
    this.forgetPasswordForm = this.formBuilder.group({
      email: ['', Validators.required],
  });
  }

  get formcontrol() { return this.forgetPasswordForm.controls; }

 async onSubmit(){
  this.submitted = true;

      if (this.forgetPasswordForm.invalid) {
          return;
      }

      this.userData=await this.userService.getprofileByEmail(this.formcontrol.email.value);
      const user:UserModel={
        id:this.userData[0].id,
        username:this.userData[0].username,
        useremail:this.userData[0].useremail,
        userpassword:this.key,
        userrole:this.userData[0].userrole,
        orgid:this.userData[0].orgid
      }
      const resetPassword:ForgotPasswordModel={
        emailaddress:this.formcontrol.email.value,
        password:this.key
      }
      this.response=await this.userService.updateUser(user);
      if(this.response.code===200){
        await this.userService.resetForgotPassword(resetPassword);
        this.forgetPasswordForm.reset();
        this.popupService.setSuccessMessage("Password Reset Successfully,Please check your email !!");
        this.popupService.activateVisibleState();
      }else{
        this.popupService.setErrorMessage("Something went wrong!!");
        this.popupService.activateVisibleState();
      }  

  }

}

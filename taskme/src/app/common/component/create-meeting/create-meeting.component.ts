import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';
import { PopupService } from '../../services/popup/popup.service';
import { CreateMeetingModel } from '../../model/meeting/meetingmodel/createmeetingmodel';
import { MeetingserviceService } from '../../services/meeting/meetingservice.service';
import { ProjectList } from '../../model/projectlist/ProjectList';
import { MemberList } from '../../model/projectlist/MemberList';
import { MeetingId } from '../../model/projectlist/MeetingId';
import { MeetingInvite } from '../../model/meeting/meetingmodel/meetinginvite';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { LoginModel } from 'src/app/common/model/login/login-model';
import { LoginstateService } from '../../services/login_state/loginstate.service';

@Component({
  selector: 'app-create-meeting',
  templateUrl: './create-meeting.component.html',
  styleUrls: ['./create-meeting.component.css']
})
export class CreateMeetingComponent implements OnInit {

  CreateMeeting: FormGroup;
  List: ProjectList[];
  memberlist: MemberList[];
  meetingid: MeetingId[];
  userid:number;
  constructor(private fb: FormBuilder,
              private Pop: PopupService,
              private meeting: MeetingserviceService,
              private router: Router,
              public dialog: MatDialog,
              private loginstateService:LoginstateService){
    if (this.loginstateService.loginState === null) {
                this.router.navigate(['/login']);
                return;
    } 
    let loginModel :LoginModel=this.loginstateService.getLoginState();
    this.userid=loginModel.id;
    this.CreateMeeting = this.fb.group({
      meetingtitle: ['', Validators.required],
      projectname: ['', Validators.required],
      meetingdiscription: ['', Validators.required],
      meetingdate: ['', Validators.required],
      meetingtime: ['', Validators.required],
      meetingtype: ['', Validators.required]
    });
 }
  async ngOnInit() {
    const response = await this.meeting.ProjectList(this.userid);
    this.List = response.data;
    const res = await this.meeting.getMemberList(this.userid);
    this.memberlist = res.data;
  }
  get form() {
    return this.CreateMeeting.controls;
  }
  async create(){
    if (this.CreateMeeting.valid){
      let meeting : CreateMeetingModel = new CreateMeetingModel();
      meeting.id = null;
      meeting.meetingtitle = this.form.meetingtitle.value;
      meeting.projectid = + this.form.projectname.value;
      meeting.creatorid = this.userid;
      meeting.meetingdescription = this.form.meetingdiscription.value;
      meeting.meetingdate = this.form.meetingdate.value;
      meeting.meetingtime = this.form.meetingtime.value;
      meeting.typeid = + this.form.meetingtype.value;
      try {
        const response = await this.meeting.CreateMeeting(meeting);
        if (response.code === 201){
          const res = await  this.meeting.getMeetingId();
          this.meetingid = res.data;
          for ( const meetid of this.meetingid){
              for (let user of this.memberlist){
                let invite: MeetingInvite = new MeetingInvite();
                invite.id = null;
                invite.userid = user.id;
                invite.meetingid = meetid.id;
                const data = await this.meeting.inviteUser(invite);
              }
          }
          // this.Pop.setSuccessMessage('Meeting is Created');
          // this.Pop.activateVisibleState();
          alert('Meeting is Ceated');
          this.dialog.closeAll();
          this.router.navigate(['/app/meeting']);
       }
      }
      catch (error){
        console.log(error);
      }
    }
  }
}

import { Component, OnInit, AfterViewInit, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import { MeetingserviceService } from '../../services/meeting/meetingservice.service';
import { MeetingModel } from '../../model/meeting/meetingmodel/meetingmodel';
import { Title } from '@angular/platform-browser';
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit, AfterViewChecked{
  meetingDetails: MeetingModel[];
  calendarEvents: any[] = [];
  calendarPlugin = [dayGridPlugin];
  events: any[] = [];
  constructor(private meetings: MeetingserviceService, private cdRef: ChangeDetectorRef) {
  }
  ngAfterViewChecked(): void {
    this.cdRef.detectChanges();
  }
   async ngOnInit() {
     this.setCalendatEvents();
    }
     setCalendatEvents(){
      const response =   this.meetings.getAllMeetingDetails(2).subscribe( res => {
        if (res){
          for (const meeting of res.data){
            this.events.push({
              title : meeting.meetingtitle ,
              start: meeting.meetingdate
            });
        }
        }
        this.calendarEvents = this.events;
      });
    }
}

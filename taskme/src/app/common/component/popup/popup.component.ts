import { Component, OnInit } from '@angular/core';
import { PopupService } from '../../services/popup/popup.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {

  constructor(public popupService: PopupService) { }

  ngOnInit(): void {
  }

  hidePopDiv() {
    this.popupService.deactivateVisibleState();
  }

}

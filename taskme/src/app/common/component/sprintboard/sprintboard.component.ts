import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SprintService } from '../../services/sprint/sprint.service';
import { IssueService } from '../../services/issue/issue.service';
import { IssueModel } from '../../model/issue/issue-model';
import { LoginstateService } from '../../services/login_state/loginstate.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sprintboard',
  templateUrl: './sprintboard.component.html',
  styleUrls: ['./sprintboard.component.css']
})
export class SprintboardComponent implements OnInit,OnDestroy{
  sprintData:[];
  gotoItem: [];
  inprogressItem: [];
  inReviewItem: [];
  doneItem:[];
  sprintId:any;
  sprintName: any;
  subscription: Subscription;
  constructor(private route: ActivatedRoute,private router:Router,private sprintService: SprintService,private issueService: IssueService,private loginstateService:LoginstateService) {
    if (this.loginstateService.loginState === null) {
      this.router.navigate(['/login']);
      return;
    } 
   }
  

  async ngOnInit(){ 
   this.subscription= this.route.queryParams.subscribe(params => {
      this.sprintId = params['sprint'];
    });

    this.sprintName= await this.sprintService.getSprintNameById(this.sprintId);
    this.gotoItem = await this.issueService.getGotoIssue(this.sprintId);
    this.inprogressItem = await this.issueService.getInprogressIssue(this.sprintId);
    this.inReviewItem = await this.issueService.getInreviewIssue(this.sprintId);
    this.doneItem = await this.issueService.getDoneIssue(this.sprintId);
  }

  onIssueView(data:IssueModel){
    const issueid = data.id;
    this.router.navigate(['/app/taskdetail'], { queryParams: { issue:issueid } })
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}

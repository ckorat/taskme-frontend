import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChangePassword } from '../../model/chnagepassword/chnagepassword';
import { PopupService } from '../../services/popup/popup.service';
import { ChangepasswordService } from '../../services/changepassword/changepassword.service';
import { Router } from '@angular/router';
import { LoginstateService } from '../../../common/services/login_state/loginstate.service';
import { LoginModel } from 'src/app/common/model/login/login-model';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit {

  changePassword: FormGroup;
  userid: number;
  constructor(private fb: FormBuilder,
              private Pop: PopupService,
              private service: ChangepasswordService,
              private router: Router,
              private loginstateService:LoginstateService) {
    if (this.loginstateService.loginState === null) {
        this.router.navigate(['/login']);
        return;
    }
    let loginModel : LoginModel= this.loginstateService.getLoginState();
    this.userid=loginModel.id;
    this.changePassword = this.fb.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      newPassword2: ['', Validators.required]
    });
   }

  ngOnInit(): void {
  }

  get form() {
    return this.changePassword.controls;
  }

  async change(){
    if (this.changePassword.valid){
      let password: ChangePassword = new ChangePassword();
      password.id = this.userid;
      password.oldPassword = this.form.oldPassword.value;
      password.newPassword = this.form.newPassword.value;
      try{
        const response = await  this.service.ChnagePassword(password);
        if (response.code === 200){
          this.Pop.setSuccessMessage(response.message);
          this.Pop.activateVisibleState();
          this.router.navigate(['/']);
        }
        else{
          this.Pop.setErrorMessage('Your Old Password is not Correct');
          this.Pop.activateVisibleState();
        }
      }
      catch (error)
      {
        console.log(error);
      }
    }
  }
}

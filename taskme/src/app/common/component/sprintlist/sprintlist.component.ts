import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from '../../../scrummaster/services/project/project.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SprintModel } from '../../model/sprint/sprint-model';
import { SprintService } from '../../services/sprint/sprint.service';
import { PopupService } from '../../services/popup/popup.service';
import { DatePipe } from '@angular/common';
import { IssueService } from '../../services/issue/issue.service';
import { IssueModel } from '../../model/issue/issue-model';
import { LoginModel } from '../../model/login/login-model';
import { LoginstateService } from '../../services/login_state/loginstate.service';
import { Subscription } from 'rxjs';
export class sprintdetails {
  details: SprintModel;
  issueSprint: [];
}
@Component({
  selector: 'app-sprintlist',
  templateUrl: './sprintlist.component.html',
  styleUrls: ['./sprintlist.component.css']
})


export class SprintlistComponent implements OnInit,OnDestroy {
  isSprintHide: boolean;
  isTaskHide: boolean;
  projectid: number;
  projectname: any;
  sprintForm: FormGroup;
  issueForm: FormGroup;
  submitted = false;
  submit = false;
  response: any;
  sprintData: sprintdetails[] = [];
  sprintItem: [];
  issueType: [];
  issueData: [];
  userRole: string;
  userId: number;
  userProjectid: any;
  subscription: Subscription;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private projectService: ProjectService,
    private router: Router, private sprintService: SprintService, private popupService: PopupService,
    private datePipe: DatePipe,private loginstateService:LoginstateService ,private issueService: IssueService) {
      if (this.loginstateService.loginState === null) {
        this.router.navigate(['/login']);
        return;
      } 
     }
  

  async ngOnInit() {
    this.isSprintHide = true;
    this.isTaskHide = true;
    this.subscription=this.route.queryParams.subscribe(params => {
      this.projectid = params['project'];
    });
    this.sprintForm = this.formBuilder.group({
      sprintname: ['', Validators.required],
      sprintdescription: ['', Validators.required],
    });
    this.issueForm = this.formBuilder.group({
      sprintId: ['', Validators.required],
      issueTypeId: ['', Validators.required],
      issuename: ['', Validators.required],
      issuedescription: ['', Validators.required],
      scrumpoint: ['', Validators.required]
    });

    let loginModel :LoginModel=this.loginstateService.getLoginState();
    this.userRole=loginModel.role;
    this.userId=loginModel.id;

    if(this.userRole==="DEV"){
      this.userProjectid=await this.projectService.getProjectByuserId(this.userId)
      this.projectid=this.userProjectid[0];
      
    }

    this.projectname = await this.projectService.getProjectNameById(this.projectid);
    this.setsprintData(this.projectid);
    this.issueType = await this.issueService.getIssueType();

  }


  get formcontrol() { return this.sprintForm.controls; }
  get formfield() { return this.issueForm.controls; }

  async onSprintSubmit() {
    this.submitted = true; 

    if (this.sprintForm.invalid) {
      return;
    }

    const sprint: SprintModel = {
      id: null,
      projectId: this.projectid,
      sprintName: this.formcontrol.sprintname.value,
      sprintDescription: this.formcontrol.sprintdescription.value,
      startDate: null,
      endDate: null,
      isActive: 0
    };

    this.response = await this.sprintService.addNewSprint(sprint);
    if (this.response.code === 201) {
      this.sprintForm.reset();
      this.setsprintData(this.projectid);
      this.submitted = false;
      this.popupService.setSuccessMessage('Successfully Sprint Added !!');
      this.popupService.activateVisibleState();
    } else {
      this.popupService.setErrorMessage(this.response.message);
      this.popupService.activateVisibleState();
    }
  }

  async onSprintStart(sprint: SprintModel) {

    const sprintData: SprintModel = {
      id: sprint.id,
      projectId: this.projectid,
      sprintName: sprint.sprintName,
      sprintDescription: sprint.sprintDescription,
      startDate:this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
      endDate: null,
      isActive: 1
    };

    this.response = await this.sprintService.updateSprint(sprintData);
    if (this.response.code === 200) {
      this.popupService.setSuccessMessage('Successfully sprint start !!');
      this.popupService.activateVisibleState();
    } else {
      this.popupService.setErrorMessage('Something Went Wrong..');
      this.popupService.activateVisibleState();
    }
  }

  async onSprintClose(sprint: SprintModel) {
    const sprintData: SprintModel = {
      id: sprint.id,
      projectId: this.projectid,
      sprintName: sprint.sprintName,
      sprintDescription: sprint.sprintDescription,
      startDate: sprint.startDate,
      endDate: this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
      isActive: 0
    };
    this.response = await this.sprintService.updateSprint(sprintData);
    if (this.response.code === 200) {
      this.popupService.setSuccessMessage('Successfully sprint closed !!');
      this.popupService.activateVisibleState();
    } else {
      this.popupService.setErrorMessage('Something Went Wrong..');
      this.popupService.activateVisibleState();
    }
  }

  async onIssueSubmit() {
    this.submit = true;

    if (this.issueForm.invalid) {
      return;
    }

    const issue: IssueModel = {
      id: null,
      sprintid: this.formfield.sprintId.value,
      issueName: this.formfield.issuename.value,
      issueDescription: this.formfield.issuedescription.value,
      creator: this.userId,
      assignee: null,
      createOn: this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
      updateOn: this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
      storyPoint: this.formfield.scrumpoint.value,
      issueTypeId: this.formfield.issueTypeId.value,
      issueStatus: "Go To"
    };

    this.response = await this.issueService.addNewIssue(issue);
    if (this.response.code === 201) {
      this.issueForm.reset();
      this.submit = false;
      window.location.reload();
      this.popupService.setSuccessMessage('Successfully Issue Added !!');
      this.popupService.activateVisibleState();
    } else {
      this.popupService.setErrorMessage(this.response.message);
      this.popupService.activateVisibleState();
    }
  }

  async setsprintData(Id: number) {
    const sprint = await this.sprintService.getSprintByproject(Id);
    this.sprintData=[];
    sprint.forEach(element => {
      let sprintdetail = new sprintdetails();
      sprintdetail.details = element;
      this.sprintData.push(sprintdetail);
    });
    for (const i of this.sprintData) {
      i.issueSprint = await this.issueService.getSprintIssue(i.details.id);
    }
  }

  onBoardView(sprint: SprintModel) {
    const sprintid = sprint.id;
    this.router.navigate(['/app/sprintboard'], { queryParams: { sprint: sprintid } });
  }
  onIssueView(issue: IssueModel) {
    const issueid = issue.id;
    this.router.navigate(['/app/taskdetail'], { queryParams: { issue: issueid } })
  }
  toggleSprintModal() {
    this.isSprintHide = !this.isSprintHide;
  }

  async toggleTaskModal() {
    if(this.isTaskHide){
      this.sprintItem = await this.sprintService.getSprintByproject(this.projectid);
    }
   
    this.isTaskHide = !this.isTaskHide;
  }

  ngOnDestroy(){
   this.subscription.unsubscribe();
  }

}

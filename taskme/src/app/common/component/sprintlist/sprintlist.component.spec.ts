import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SprintlistComponent } from './sprintlist.component';

describe('SprintlistComponent', () => {
  let component: SprintlistComponent;
  let fixture: ComponentFixture<SprintlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SprintlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SprintlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

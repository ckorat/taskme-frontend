import { Component, OnInit, OnDestroy } from '@angular/core';
import { IssueService } from '../../services/issue/issue.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginModel } from '../../model/login/login-model';
import { LoginstateService } from '../../services/login_state/loginstate.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IssueModel } from '../../model/issue/issue-model';
import { DatePipe } from '@angular/common';
import { PopupService } from '../../services/popup/popup.service';
import { CommentModel } from '../../model/comment/comment-model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-taskdetail',
  templateUrl: './taskdetail.component.html',
  styleUrls: ['./taskdetail.component.css']
})
export class TaskdetailComponent implements OnInit,OnDestroy {

  issueForm:FormGroup;
  commentForm:FormGroup;
  issueDetails:[];
  issueId: any;
  userRole:string;
  isEdit:boolean;
  isDisplay:boolean;
  issueData: any;
  issue:IssueModel;
  userId: number;
  submitted = false;
  commentDetails: [];
  subscription: Subscription;
  
  constructor(private router:Router,private issueService:IssueService,private route:ActivatedRoute,private loginstateService:LoginstateService, private formBuilder: FormBuilder, private datePipe: DatePipe,private popupService:PopupService) {
    if (this.loginstateService.loginState === null) {
      this.router.navigate(['/login']);
      return;
    } 
   }
  

  async ngOnInit(){
    let loginModel :LoginModel=this.loginstateService.getLoginState();
    this.userRole=loginModel.role;
    this.userId=loginModel.id;
    
    this.subscription=this.route.queryParams.subscribe(params => {
      this.issueId = params['issue'];
    });
    this.isDisplay=true;
    this.isEdit=false;

    this.issueData= await this.issueService.getIssue(this.issueId);
   
    await this.onLoad(this.issueId);
    
    this.issueForm = this.formBuilder.group({
      issuedescription: [this.issue.issueDescription]
    });
    this.commentForm = this.formBuilder.group({
      comment: ['',Validators.required]
    });

    this.issueDetails=await this.issueService.getIssueDetail(this.issueId);
    this.commentDetails=await this.issueService.getComment(this.issueId);
  }

  get formcontrol() {
    return this.issueForm.controls;
  }

  get formfield() {
    return this.commentForm.controls;
  }

  async onLoad(Id:number){
    this.issueData= await this.issueService.getIssue(Id);
    this.issue= this.issueData[0];
  }

  onEdit(){
    this.isDisplay=false;
    this.isEdit=true;
  }
  async issueSave(){

    const issueModel:IssueModel={
      id:this.issue.id,
      sprintid:this.issue.sprintid,
      issueName:this.issue.issueName,
      issueDescription: this.formcontrol.issuedescription.value,
      creator:this.issue.creator,
      assignee:this.issue.assignee,
      createOn:this.issue.createOn,
      updateOn:this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
      storyPoint:this.issue.storyPoint,
      issueTypeId:this.issue.issueTypeId,
      issueStatus:this.issue.issueStatus
    }

    let resp = await this.issueService.updateIssue(issueModel);
    if (resp.code === 200) {
      this.isDisplay=true;
      this.isEdit=false;
      this.issueDetails=await this.issueService.getIssueDetail(this.issueId);
      this.popupService.setSuccessMessage('Your issue updated successfully.');
    } else {
      this.popupService.setErrorMessage(resp.message);
    }
    this.popupService.activateVisibleState();
  }

  async onComment(){

    this.submitted = true;

        if (this.commentForm.invalid) {
            return;
        }

    const commentModel:CommentModel={
      id: null,
      issueid: this.issueId,
      commenter: this.userId,
      comment:this.formfield.comment.value,
      commentdatetime:this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss')
    }

    let resp = await this.issueService.addNewComment(commentModel);
    if (resp.code === 201) {
      this.commentForm.reset();
      this.submitted=false;
      this.commentDetails=await this.issueService.getComment(this.issueId);
    } else {
      this.popupService.setErrorMessage(resp.message);
    }
    this.popupService.activateVisibleState();

  }

  async onInprogress(){

    const issueModel:IssueModel={
      id:this.issue.id,
      sprintid:this.issue.sprintid,
      issueName:this.issue.issueName,
      issueDescription: this.issue.issueDescription,
      creator:this.issue.creator,
      assignee:this.issue.assignee,
      createOn:this.issue.createOn,
      updateOn:this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
      storyPoint:this.issue.storyPoint,
      issueTypeId:this.issue.issueTypeId,
      issueStatus:"In Progress"
    }

    let resp = await this.issueService.updateIssue(issueModel);
    if (resp.code === 200) {
      this.issueDetails=await this.issueService.getIssueDetail(this.issueId);
      this.popupService.setSuccessMessage('Your issue updated successfully.');
    } else {
      this.popupService.setErrorMessage(resp.message);
    }
    this.popupService.activateVisibleState();
  }

  async onInreview(){

    const issueModel:IssueModel={
      id:this.issue.id,
      sprintid:this.issue.sprintid,
      issueName:this.issue.issueName,
      issueDescription: this.issue.issueDescription,
      creator:this.issue.creator,
      assignee:this.issue.assignee,
      createOn:this.issue.createOn,
      updateOn:this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
      storyPoint:this.issue.storyPoint,
      issueTypeId:this.issue.issueTypeId,
      issueStatus:"In Review"
    }

    let resp = await this.issueService.updateIssue(issueModel);
    if (resp.code === 200) {
      this.issueDetails=await this.issueService.getIssueDetail(this.issueId);
      this.popupService.setSuccessMessage('Your issue in review successfully.');
    } else {
      this.popupService.setErrorMessage(resp.message);
    }
    this.popupService.activateVisibleState();
  }

  async ondone(){

 const issueModel:IssueModel={
      id:this.issue.id,
      sprintid:this.issue.sprintid,
      issueName:this.issue.issueName,
      issueDescription: this.issue.issueDescription,
      creator:this.issue.creator,
      assignee:this.issue.assignee,
      createOn:this.issue.createOn,
      updateOn:this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
      storyPoint:this.issue.storyPoint,
      issueTypeId:this.issue.issueTypeId,
      issueStatus:"Done"
    }

    let resp = await this.issueService.updateIssue(issueModel);
    if (resp.code === 200) {
      this.issueDetails=await this.issueService.getIssueDetail(this.issueId);
      this.popupService.setSuccessMessage('Your issue done successfully.');
    } else {
      this.popupService.setErrorMessage(resp.message);
    }
    this.popupService.activateVisibleState();
  }

  async onAssignee(){
    const issueModel:IssueModel={
      id:this.issue.id,
      sprintid:this.issue.sprintid,
      issueName:this.issue.issueName,
      issueDescription: this.issue.issueDescription,
      creator:this.issue.creator,
      assignee:this.userId,
      createOn:this.issue.createOn,
      updateOn:this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
      storyPoint:this.issue.storyPoint,
      issueTypeId:this.issue.issueTypeId,
      issueStatus:"To Do"
    }

    let resp = await this.issueService.updateIssue(issueModel);
    if (resp.code === 200) {
      this.issueDetails=await this.issueService.getIssueDetail(this.issueId);
      this.popupService.setSuccessMessage('Your issue updated successfully.');
    } else {
      this.popupService.setErrorMessage(resp.message);
    }
    this.popupService.activateVisibleState();
  }
  ngOnDestroy(){
   this.subscription.unsubscribe();
  }
}

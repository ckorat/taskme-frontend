import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactusModel } from '../../model/contactus/contactus-model';
import { ContactusService } from '../../services/contactus/contactus.service';
import { PopupService } from '../../services/popup/popup.service';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {
  
  contactusForm: FormGroup;
  submitted = false;
  response: any;

  constructor(private formBuilder: FormBuilder,private contactusService:ContactusService,private popupService:PopupService) { }

  ngOnInit() {
      this.contactusForm = this.formBuilder.group({
          name: ['', Validators.required],
          email: ['', Validators.required],
          message: ['', Validators.required]
      });
  }

  get formcontrol() { return this.contactusForm.controls; }

  async onSubmit() {
      this.submitted = true;

      if (this.contactusForm.invalid) {
          return;
      }

      const contactus:ContactusModel={
        name:this.formcontrol.name.value,
        emailaddress:this.formcontrol.email.value,
        message:this.formcontrol.message.value
      }

      await this.contactusService.sendContactUs(contactus);
      this.contactusForm.reset();
      this.submitted = false;
      this.popupService.setSuccessMessage('Successfully Sent !!');
      this.popupService.activateVisibleState();
  }

}

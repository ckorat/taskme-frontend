import { Component, OnInit } from '@angular/core';
import { menus } from '../../navconfig';
import { LoginstateService } from '../../services/login_state/loginstate.service';
import { Router } from '@angular/router';
import { LoginModel } from '../../model/login/login-model';

@Component({
  selector: 'app-navbar',
   templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  menu = menus;
  userName:string;
  userRole:string;
  constructor(private loginstateService:LoginstateService,private route:Router) { }

  ngOnInit(): void {
    this.getLoginState();
  }

  getMenutoDisplay(): any[] {
    if (this.checkLoginState('SCRUM')) {
      return this.menu.filter(it => it.url.includes('/scrummaster'));
    } else if (this.checkLoginState('DEV')) {
      return this.menu.filter(it => it.url.includes('/developer'));
    }   
  }

  logout() {
    sessionStorage.removeItem("TOKEN");
    this.loginstateService.destroySession();
    this.route.navigate(['/index']);
  }

  checkLoginState(role: string): boolean {
    let loginModel :LoginModel=this.loginstateService.getLoginState();
    return (loginModel.role=== role);
  }
  getLoginState(){
    let loginModel :LoginModel=this.loginstateService.getLoginState();
    this.userName=loginModel.username;
    if(loginModel.role==='SCRUM'){
      this.userRole="Scrum Master"
    }else{
      this.userRole="Developer"
    }
  }

}

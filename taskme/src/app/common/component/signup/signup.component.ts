import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OrganizationModel } from '../../model/organization/organization-model';
import { UserService } from '../../services/user/user.service';
import { PopupService } from '../../services/popup/popup.service';
import { UserModel } from '../../model/user/user-model';
import { Router, ActivatedRoute } from '@angular/router';
import { UserProjectModel } from '../../model/userproject/userproject-model';
import { ProjectService } from '../../../scrummaster/services/project/project.service';
import { InviteDeveloperModel } from 'src/app/scrummaster/model/invitedeveloper/invitedeveloper-model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit,OnDestroy {
  signupForm: FormGroup;
  submitted = false;
  orgid:any;
  projectid:any;
  
  Checkbox: boolean;
  response: any;
  userResponse: any;
  userid: any;
  subscriptionProject: Subscription;
  subscriptionOrg: Subscription;
  constructor(private formBuilder: FormBuilder,private userService:UserService,private popupService:PopupService,private router:Router,private route:ActivatedRoute,
              private projectService:ProjectService) { }
 

  ngOnInit(): void {
    this.subscriptionProject=this.route.queryParams
      .subscribe(params => {  
        this.projectid = params.project;
      });
      this.subscriptionOrg=this.route.queryParams
      .subscribe(params => {  
        this.orgid = params.org;
      });
    
    this.Checkbox=false;
    this.signupForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      orgname:[''],
      ckborg:[false]
  });
 
  }

  get formcontrol() { return this.signupForm.controls; }

   async onSubmit() {
    
        this.submitted = true; 

        if (this.signupForm.invalid) {
            return;
        }

        if(this.Checkbox===false){
          if(this.projectid===undefined && this.orgid===undefined){
            const user:UserModel={
              id:null,
              username:this.formcontrol.name.value,
              useremail:this.formcontrol.email.value,
              userpassword:this.formcontrol.password.value,
              userrole:"DEV",
              orgid:null
            }

            this.userResponse= await this.userService.addNewUser(user);
            if(this.userResponse.code===201){
             this.signupForm.reset();
             this.submitted = false;
             this.router.navigate(['/login']);
            }else{
             this.popupService.setErrorMessage(this.userResponse.message);
             this.popupService.activateVisibleState();
            }
          }else{

            const invitedeveloper:InviteDeveloperModel={
              id:0,
              useremail:this.formcontrol.email.value,
              projectid:0,
              isaccept:1
            }
            
            const userproject: UserProjectModel = {
              id: null,
              projectId:this.projectid,
              userId:0
            };
            const user:UserModel={
              id:null,
              username:this.formcontrol.name.value,
              useremail:this.formcontrol.email.value,
              userpassword:this.formcontrol.password.value,
              userrole:"DEV",
              orgid:this.orgid
            }
            this.response = await this.userService.addNewUser(user);
            if (this.response.code === 201) {
             this.userid=await this.userService.getprofileIdByEmail(this.formcontrol.email.value);
             await this.userService.updateInviteDeveloperByUserId(invitedeveloper);
             userproject.userId = this.userid[0];
             this.userResponse= await this.projectService.addUserProject(userproject);
             if(this.userResponse.code===201){
              this.signupForm.reset();
              this.submitted = false;
              this.router.navigate(['/login']);
             }else{
              this.popupService.setErrorMessage(this.userResponse.message);
              this.popupService.activateVisibleState();
             }
            } else {
              this.popupService.setErrorMessage(this.response.message);
              this.popupService.activateVisibleState();
            }
          }
        }else{
           const org: OrganizationModel = {
            id: null,
            orgname:this.formcontrol.orgname.value
          };
          const user:UserModel={
            id:null,
            username:this.formcontrol.name.value,
            useremail:this.formcontrol.email.value,
            userpassword:this.formcontrol.password.value,
            userrole:"SCRUM",
            orgid:null
          }
          this.response = await this.userService.addNewOrganization(org);
          if (this.response.code === 200) {
           this.orgid=await this.userService.getOrganizationIdByName(this.formcontrol.orgname.value);
           user.orgid = this.orgid[0];
           this.userResponse= await this.userService.addNewUser(user);
           if(this.userResponse.code===201){
            this.signupForm.reset();
            this.submitted = false;
            this.router.navigate(['/login']);
           }else{
            this.popupService.setErrorMessage(this.userResponse.message);
            this.popupService.activateVisibleState();
           }
          } else {
            this.popupService.setErrorMessage(this.response.message);
            this.popupService.activateVisibleState();
          }
        }
        }
        ngOnDestroy() {
         this.subscriptionProject.unsubscribe();
         this.subscriptionOrg.unsubscribe();
        }
    }



import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginstateService } from '../../services/login_state/loginstate.service';
import { AuthenticateService } from '../../services/authenticate/authenticate.service';
import { LoginModel } from '../../model/login/login-model';
import { UserService } from '../../services/user/user.service';
import { ProjectService } from '../../../scrummaster/services/project/project.service';
import { UserModel } from '../../model/user/user-model';
import { UserProjectModel } from '../../model/userproject/userproject-model';
import { InviteDeveloperModel } from '../../../scrummaster/model/invitedeveloper/invitedeveloper-model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit,OnDestroy {

  loginForm: FormGroup;
  submitted = false;
  flag = true;
  projectid:any;
  orgid:any;
  isOrgNull: any;
  userId: any;
  userData:UserModel;
  subscriptionProject: Subscription;
  subscriptionOrg: Subscription;
  constructor(private formBuilder: FormBuilder,private router:Router,private loginstateService:LoginstateService,private authenticateService:AuthenticateService,
              private route:ActivatedRoute,private userService:UserService,private projectService:ProjectService) { }
  

  ngOnInit(): void {

    this.subscriptionProject=this.route.queryParams
    .subscribe(params => {  
      this.projectid = params.project;
    });
    this.subscriptionOrg=this.route.queryParams
    .subscribe(params => {  
      this.orgid = params.org;
    });

    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }
  get formcontrol() { return this.loginForm.controls; }

 async onSubmit(){
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }
    let loginModel: LoginModel;
    try {
      const logStatus = await this.authenticateService.authenticate(this.formcontrol.email.value,
        this.formcontrol.password.value);
        
      sessionStorage.setItem("TOKEN", logStatus.headers.get('Authorization'));

      if(this.projectid!==undefined || this.orgid!==undefined){
        this.isOrgNull= await this.userService.getorgIdByEmail(this.formcontrol.email.value);
        this.userId=await this.userService.getprofileIdByEmail(this.formcontrol.email.value);
        this.userData=await this.userService.getprofileByEmail(this.formcontrol.email.value);
        if(this.isOrgNull[0]===null){

          const invitedeveloper:InviteDeveloperModel={
            id:0,
            useremail:this.formcontrol.email.value,
            projectid:0,
            isaccept:1
          }
  
          const userproject: UserProjectModel = {
                id: null,
                projectId:this.projectid,
                userId:0
              };
              const user:UserModel={
                id:this.userData[0].id,
                username:this.userData[0].username,
                useremail:this.userData[0].useremail,
                userpassword:this.userData[0].userpassword,
                userrole:this.userData[0].userrole,
                orgid:this.orgid
              }
              userproject.userId=this.userId[0];
             await this.projectService.addUserProject(userproject);
             await this.userService.updateInviteDeveloperByUserId(invitedeveloper);
             await this.userService.updateUser(user);
        }
      }
      loginModel = logStatus.body;
    } catch (error) {
      this.flag = false;
    }

    if (loginModel != null) {
      if (loginModel.role === 'SCRUM' || loginModel.role === 'DEV') {
        this.loginstateService.createSession({
          role: loginModel.role,id: loginModel.id,username: loginModel.username, orgid: loginModel.orgid
        });
        this.routeByRole(loginModel.role);
      }
    } else {
      this.flag = false;
    }

  }
  routeByRole(role: any) {
    role = role.toLowerCase();
    if (role === 'scrum') {
      this.router.navigate(['/app/projects']);
    } else if (role === 'dev') {
      this.router.navigate(['/app/mytask']);
    } 
  }
  ngOnDestroy(){
    this.subscriptionProject.unsubscribe();
    this.subscriptionOrg.unsubscribe();
  }

}

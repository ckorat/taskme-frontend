export class SprintModel {
   id:number;
   projectId:number;
   sprintName: string;
   sprintDescription: string;
   startDate: string;
   endDate: string;
   isActive: number;
}
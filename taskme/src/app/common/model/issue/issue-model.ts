export class IssueModel {
    id:number;
    sprintid:number;
    issueName:string;
    issueDescription: string;
    creator:number;
    assignee:number;
    createOn:string;
    updateOn:string;
    storyPoint:number;
    issueTypeId:number;
    issueStatus:string;
 }
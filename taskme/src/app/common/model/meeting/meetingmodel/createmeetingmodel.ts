export class CreateMeetingModel{
    id: number;
    projectid: number;
    creatorid: number;
    meetingtitle: string;
    meetingdescription: string;
    meetingdate: string;
    meetingtime: string;
    typeid: number;
}

export class CommentModel{
    id: number;
    issueid: number;
    commenter: number;
    comment:string;
    commentdatetime:string;
}
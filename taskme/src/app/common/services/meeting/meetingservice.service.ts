import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MeetingModel } from '../../model/meeting/meetingmodel/meetingmodel';
import { Observable } from 'rxjs';
import { CreateMeetingModel } from '../../model/meeting/meetingmodel/createmeetingmodel';
import { MemberList } from '../../model/projectlist/MemberList';
import { MeetingInvite } from '../../model/meeting/meetingmodel/meetinginvite';

@Injectable({
  providedIn: 'root'
})
export class MeetingserviceService {
  url = 'http://localhost:8081/common-service/meetings/user/';
  createmeeting = 'http://localhost:8081/common-service/meetings/';
  projectlist = 'http://localhost:8081/issue-service/projects/list/';
  memberlist = 'http://localhost:8081/issue-service/projects/member/';
  meetingid = 'http://localhost:8081/issue-service/projects/meetingid';
  invite = 'http://localhost:8081/common-service/meetings/invite';
  meetingDetails: MeetingModel[];
  constructor(private http: HttpClient){}

   getAllMeetingDetails(id: number): Observable<any>{
    return this.http.get<any>(this.url + id);
  }
  async CreateMeeting(meeting: CreateMeetingModel): Promise<any>{
    return await this.http.post<any>(this.createmeeting, meeting).toPromise();
  }

  async ProjectList(id: number): Promise<any>{
    return await this.http.get<any>(this.projectlist + id).toPromise();
  }

  async getMemberList(id: number): Promise<any>{
    return await this.http.get<any>(this.memberlist + id).toPromise();
  }

  async getMeetingId(): Promise<any>{
    return await this.http.get<any>(this.meetingid).toPromise();
  }

  async inviteUser(user: MeetingInvite): Promise<any>{
    return await this.http.post<any>(this.invite, user).toPromise();
  }
}

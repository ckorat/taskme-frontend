import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  intercept(request, next) {
    const token: string = sessionStorage.getItem('TOKEN');
    if (token) {
        request = request.clone({
          setHeaders: {
            Authorization: token
          }
        });
    }
    return next.handle(request);
}
}

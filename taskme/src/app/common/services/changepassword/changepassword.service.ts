import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ChangePassword } from '../../model/chnagepassword/chnagepassword';
@Injectable({
  providedIn: 'root'
})
export class ChangepasswordService {
  url = 'http://localhost:8081/profile-service/profiles/password/';
  constructor(private http: HttpClient) {}
  async ChnagePassword(password: ChangePassword): Promise<any>{
    return await this.http.put<any>(this.url, password).toPromise();
  }
}

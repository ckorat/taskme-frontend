import { Injectable } from '@angular/core';
import { LoginModel } from '../../model/login/login-model';
import { PopupService } from '../popup/popup.service';

@Injectable({
  providedIn: 'root'
})
export class LoginstateService {
  loginState: LoginModel;

  constructor(private popupService: PopupService) { }

  createSession({ id,username,role,orgid}) {
    const state: LoginModel = {id,role,username,orgid};
    sessionStorage.setItem('loginState', JSON.stringify(state));
  }

  destroySession() {
    sessionStorage.removeItem('loginState');
    this.loginState = null;
  }

  getLoginState() {
    return JSON.parse(sessionStorage.getItem('loginState'));
  }

  isLoginStateValidOfUser(currentUser: string): boolean {
    // currentUser = currentUser.toLowerCase();
    this.loginState = this.getLoginState();
    
    if (this.loginState === null ||
      (this.loginState !== null && this.loginState.role !== currentUser)) {
        this.popupService.setErrorMessage('You must login first !!');
        this.popupService.activateVisibleState();
      return true;
    } else {
     
      return false;
    }
  }
}

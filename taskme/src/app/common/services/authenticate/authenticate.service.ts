import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {
  private urlPath = 'http://localhost:8081/';

  constructor(private http: HttpClient) {}

  async authenticate(username: string, password: string): Promise<any> {
    return await this.http.post(this.urlPath + 'auth/', { username, password }, { observe: 'response' }).toPromise();
  }
}

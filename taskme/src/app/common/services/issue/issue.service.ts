import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IssueModel } from '../../model/issue/issue-model';
import { CommentModel } from '../../model/comment/comment-model';
import { promise } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class IssueService {
  urlPath = 'http://localhost:8081/issue-service/issues';
  url = 'http://localhost:8081/issue-service';
  constructor(private http: HttpClient) { }

  async getIssue(id:number): Promise<any> {
    const issues = await this.http.get(this.urlPath + '/').toPromise();
    const issue= issues["data"];  
    return issue.filter(data=>data.id===+id);

}
  async getIssueType(): Promise<any> {
    const sprinttypes = await this.http.get(this.urlPath + '/type').toPromise();
    return sprinttypes["data"];  
}

async getIssueDetail(id:number): Promise<any> {
  const issuedetails = await this.http.get(this.urlPath + '/issue/'+id).toPromise();
  const issuedetail= issuedetails["data"]; 
  return issuedetail.filter(data => data.id === +id); 
}

async addNewIssue(data:IssueModel) : Promise<any>{
  return await this.http.post(this.urlPath+'/', data).toPromise();
}

async updateIssue(data:IssueModel) : Promise<any>{
  return await this.http.put(this.urlPath+'/', data).toPromise();
}
async getSprintIssue(Id:number): Promise<any> {
    const sprintIssues = await this.http.get<IssueModel>(this.urlPath + '/sprints/'+Id+'/issues/').toPromise();
    const sprintIssue= sprintIssues["data"]; 
    return sprintIssue.filter(data => data.sprintid === +Id);
}
async getAssigneeIssue(sprintId:number,assigneeId:number):Promise<any>{
    const issues=await this.getSprintIssue(sprintId);
    return issues.filter(data=>data.assignee == +assigneeId);
}

async getAssigneeGotoIssue(sprintId:number,assigneeId:number):Promise<any>{
  const gotoIssues= await this.getAssigneeIssue(sprintId,assigneeId);
  return gotoIssues.filter(data => data.issueStatus === "Go To");
}
async getAssigneeInprogressIssue(sprintId:number,assigneeId:number):Promise<any>{
  const inprogressIssues= await this.getAssigneeIssue(sprintId,assigneeId);
  return inprogressIssues.filter(data => data.issueStatus === "In Progress");
}
async getAssigneeInreviewIssue(sprintId:number,assigneeId:number):Promise<any>{
  const inreviewIssues= await this.getAssigneeIssue(sprintId,assigneeId);
  return inreviewIssues.filter(data => data.issueStatus === "In Review");
}
async getAssigneeDoneIssue(sprintId:number,assigneeId:number):Promise<any>{
  const doneIssues= await this.getAssigneeIssue(sprintId,assigneeId);
  return doneIssues.filter(data => data.issueStatus === "Done");
}

async getGotoIssue(Id:number): Promise<any> {
  const gotoIssue= await this.getSprintIssue(Id);
  return gotoIssue.filter(data => data.issueStatus === "Go To");
}
async getInprogressIssue(Id:number): Promise<any> {
  const inprogressIssue= await this.getSprintIssue(Id); 
  return inprogressIssue.filter(data => data.issueStatus === "In Progress");
}
async getInreviewIssue(Id:number): Promise<any> {
  const inreviewIssue= await this.getSprintIssue(Id); 
  return inreviewIssue.filter(data => data.issueStatus === "In Review");
}
async getDoneIssue(Id:number): Promise<any> {
  const doneIssue= await this.getSprintIssue(Id);  
  return doneIssue.filter(data => data.issueStatus === "Done");
}

async addNewComment(data:CommentModel) : Promise<any>{
  return await this.http.post(this.url+'/comments/', data).toPromise();
}

async getComment(id:number): Promise<any> {
  const comments = await this.http.get(this.url+ '/comments/'+id).toPromise();
  return comments["data"];  
}

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContactusModel } from '../../model/contactus/contactus-model';

@Injectable({
  providedIn: 'root'
})
export class ContactusService {
  urlPath = 'http://localhost:8081/email-service/email';
  constructor(private http: HttpClient) { }

  async sendContactUs(data:ContactusModel) : Promise<any>{
    return await this.http.post(this.urlPath+'/contactus', data).toPromise();
  }
}

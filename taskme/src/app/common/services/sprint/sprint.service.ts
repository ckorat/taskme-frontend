import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SprintModel } from '../../model/sprint/sprint-model';

@Injectable({
  providedIn: 'root'
})
export class SprintService {
  urlPath = 'http://localhost:8081/issue-service/sprints';
  constructor(private http: HttpClient) { }

  async getSprint(): Promise<any> {
    return  await this.http.get(this.urlPath + '/').toPromise();
}
async getSprintByproject(Id: number):Promise<any> {
  const sprintprojects= await this.http.get<SprintModel>(this.urlPath + '/').toPromise();
  const sprintproject=sprintprojects["data"]; 
  return sprintproject.filter(data => data.projectId === +Id);
}
async getActiveSprint(Id:number):Promise<any>{
  const activesprints= await this.http.get(this.urlPath + '/active').toPromise();
  const activesprint=activesprints["data"];
  return activesprint.filter(data=> data.projectId==+Id);
}
  async addNewSprint(data:SprintModel) : Promise<any>{
      return await this.http.post(this.urlPath+'/', data).toPromise();
  }

  async updateSprint(updatedSprint: SprintModel): Promise<any> {
      return await this.http.put(this.urlPath+'/', updatedSprint).toPromise();
  }

  async getSprintNameById(Id: number): Promise<any> {
    const sprints = await this.http.get(this.urlPath+'/'+Id).toPromise();
    const sprint = sprints["data"];
    return sprint.map(data => data.sprintName);
  }

  async getActiveSprintByProjectId(Id: number): Promise<any> {
    const sprints = await this.http.get(this.urlPath+'/active/'+Id).toPromise();
    return sprints["data"];
  }
  

}

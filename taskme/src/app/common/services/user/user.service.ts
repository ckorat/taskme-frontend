import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OrganizationModel } from '../../model/organization/organization-model';
import { UserModel } from '../../model/user/user-model';
import { UserProjectModel } from '../../model/userproject/userproject-model';
import { promise } from 'protractor';
import { InviteDeveloperModel } from '../../../scrummaster/model/invitedeveloper/invitedeveloper-model';
import { ForgotPasswordModel } from '../../model/forgetpassword/forgetpassword-model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  urlPath = 'http://localhost:8081/profile-service/profiles';
  url = 'http://localhost:8081/email-service/email';
  constructor(private http: HttpClient) { }

  async addNewOrganization(data: OrganizationModel): Promise<any> {
    return await this.http.post(this.urlPath + '/organization', data).toPromise();
  }

  async getOrganizationIdByName(name:string): Promise<any> {
    const organizations = await this.http.get(this.urlPath+'/organization/'+name).toPromise();
    const organization = organizations["data"];
    return organization.map(data => data.id);
  }
  async resetForgotPassword(data:ForgotPasswordModel) : Promise<any>{
    return await this.http.post(this.url+'/reset', data).toPromise();
  }
 

  async addNewUser(data: UserModel): Promise<any> {
    return await this.http.post(this.urlPath + '/', data).toPromise();
  }

  async addInviteDeveloper(data: InviteDeveloperModel): Promise<any> {
    return await this.http.post(this.urlPath + '/invite', data).toPromise();
  }

  async updateUser(data: UserModel): Promise<any> {
    return await this.http.put(this.urlPath + '/', data).toPromise();
  }

  async getprofileIdByEmail(email:string): Promise<any> {
    const profiles = await this.http.get(this.urlPath+'/email/'+email).toPromise();
    const profile = profiles["data"];
    return profile.map(data => data.id);
  }

  async getorgIdByEmail(email:string): Promise<any> {
    const orgs = await this.http.get(this.urlPath+'/email/'+email).toPromise();
    const org = orgs["data"];
    return org.map(data => data.orgid);
  }

  async getprofileByEmail(email:string): Promise<any> {
    const orgs = await this.http.get(this.urlPath+'/email/'+email).toPromise();
    return orgs["data"];
  }
  async getdeveloperByProjectId(Id:number): Promise<any> {
    const developers = await this.http.get(this.urlPath+'/invite/'+Id).toPromise();
    return developers["data"];
  }
  async removeDeveloperByProjectId(useremail:string,projectid:number):Promise<any>{
    return await this.http.delete(this.urlPath + '/invite/?useremail='+useremail+'&projectid='+projectid).toPromise();
  }
  async updateInviteDeveloperByUserId(data: InviteDeveloperModel): Promise<any> {
    return await this.http.put(this.urlPath + '/invite/', data).toPromise();
  }
  
}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PopupService {
  visibleState: boolean;
  successMessage: string;
  errorMessage: string;

  constructor() {
    this.visibleState = false;
    this.successMessage = '';
    this.errorMessage = '';
  }

   activateVisibleState() : void {
    this.visibleState = true;
  }

  deactivateVisibleState() : void {
    this.visibleState = false;
  }

   setSuccessMessage(msg: string) : void {
    this.errorMessage = '';
    this.successMessage = msg;
  }

  setErrorMessage(msg: string) : void {
    this.successMessage = '';
    this.errorMessage = msg;
  }

  getVisibleState():boolean {
    return this.visibleState;
  }

  getSuccessMessage():string {
    return this.successMessage;
  }

  getErrorMessage():string {
    return this.errorMessage;
  }
}

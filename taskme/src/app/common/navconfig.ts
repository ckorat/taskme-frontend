export const menus = [
    {
      url: '/scrummaster',
      menulist: [
        {
          label: 'Projects',
          routerLink: '/app/projects'
        },
        {
          label: 'Meetings',
          routerLink: '/app/meeting'
        },
        {
          label: 'Reports',
          routerLink: '/app/chart'
        },
        {
          label: 'Project Teams',
          routerLink: '/app/managedeveloper'
        },
        {
          label: 'Setting',
          routerLink: '/app/setting'
        },
        {
          label: 'Logout',
          routerLink: '/'
        }

      ]
    },
    {
        url: '/developer',
        menulist: [
          {
            label: 'Your Work',
            routerLink: '/app/mytask'
          },
          {
            label: 'Project Sprint',
            routerLink: '/app/sprintlist'
          },
          {
            label: 'Meetings',
            routerLink: '/app/meeting'
          },
          {
            label: 'Setting',
            routerLink: '/app/setting'
          },
          {
            label: 'Logout',
            routerLink: '/'
          }
        ]
      },
     
];
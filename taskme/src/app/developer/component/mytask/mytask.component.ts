import { Component, OnInit } from '@angular/core';
import { LoginModel } from 'src/app/common/model/login/login-model';
import { LoginstateService } from '../../../common/services/login_state/loginstate.service';
import { ProjectService } from '../../../scrummaster/services/project/project.service';
import { SprintService } from '../../../common/services/sprint/sprint.service';
import { IssueService } from '../../../common/services/issue/issue.service';
import { IssueModel } from '../../../common/model/issue/issue-model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mytask',
  templateUrl: './mytask.component.html',
  styleUrls: ['./mytask.component.css']
})
export class MytaskComponent implements OnInit {
  userid: number;
  project: any;
  projectId: any;
  sprint: any;
  sprintId: number;
  issues: any;
  assigneeGotoIssue: [];
  assigneeInprogressIssue: [];
  assigneeInreviewIssue: [];
  assigneeDoneIssue: [];
  projectName: any;
  sprintName: any;

  constructor(private loginstateService:LoginstateService,private projectService:ProjectService,private sprintService:SprintService,
              private issueService:IssueService,private router:Router) { }

  async ngOnInit(){
    let loginModel :LoginModel=this.loginstateService.getLoginState();
    this.userid=loginModel.id;

    await this.onLoad();
  }

  async onLoad(){
    this.project=await this.projectService.getProjectByuserId(this.userid);
    this.projectId=this.project[0];
    this.projectName=await this.projectService.getProjectNameById(this.projectId);
    this.sprint=await this.sprintService.getActiveSprintByProjectId(this.projectId);
    this.sprintId=this.sprint[0].id;
    this.sprintName=this.sprint[0].sprintName;

    this.assigneeGotoIssue= await this.issueService.getAssigneeGotoIssue(this.sprintId,this.userid);
    this.assigneeInprogressIssue= await this.issueService.getAssigneeInprogressIssue(this.sprintId,this.userid);
    this.assigneeInreviewIssue= await this.issueService.getAssigneeInreviewIssue(this.sprintId,this.userid);
    this.assigneeDoneIssue= await this.issueService.getAssigneeDoneIssue(this.sprintId,this.userid);

  }

  onIssueView(data:IssueModel){
    const issueid = data.id;
    this.router.navigate(['/app/taskdetail'], { queryParams: { issue:issueid } })
  }

}

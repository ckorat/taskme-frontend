import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MytaskComponent } from './component/mytask/mytask.component';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationComponent } from '../common/component/application/application.component';
import { FormsModule } from '@angular/forms';


const routes: Routes = [
 
  { path: 'app', component: ApplicationComponent,children: [
  
    { path: 'mytask', component: MytaskComponent, outlet:'content'},
   
  ]}
];
@NgModule({
  declarations: [MytaskComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot(routes),
    RouterModule
  ]
})
export class DeveloperModule { }

import { Component } from '@angular/core';
import { PopupService } from './common/services/popup/popup.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'taskme';
  constructor(public popupService: PopupService) {
  }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProjectService } from '../../services/project/project.service';
import { Router } from '@angular/router';
import { LoginstateService } from 'src/app/common/services/login_state/loginstate.service';
import { LoginModel } from 'src/app/common/model/login/login-model';
import { UserService } from '../../../common/services/user/user.service';
import { UserProjectModel } from '../../../common/model/userproject/userproject-model';
import { InviteModel } from '../../model/invite/invite-model';
import { InviteService } from '../../services/invite/invite.service';
import { InviteDeveloperModel } from '../../model/invitedeveloper/invitedeveloper-model';
import { ThrowStmt } from '@angular/compiler';
import { UserModel } from '../../../common/model/user/user-model';
import { PopupService } from '../../../common/services/popup/popup.service';

@Component({
  selector: 'app-managedeveloper',
  templateUrl: './managedeveloper.component.html',
  styleUrls: ['./managedeveloper.component.css']
})
export class ManagedeveloperComponent implements OnInit {
  searchForm: FormGroup;
  submitted = false;
  inviteForm: FormGroup;
  submit = false;
  isHide: boolean;
  userid: number;
  activeProjectData:[];
  developerData: [];
  removeResponse: any;
  projectId: any;
  isUser:number;
  projectName: string;
  userName: string;
  orgId: number;
  UserId: any;
  userData: UserModel;
  userId: number;

  constructor(private formBuilder: FormBuilder,private projectService: ProjectService,private route:Router,
    private loginstateService:LoginstateService,private userService:UserService,private inviteService:InviteService,private popupService:PopupService) { }

  ngOnInit() {
    this.isHide = true;

    this.searchForm = this.formBuilder.group({
      project: ['', Validators.required]     
  });
  this.inviteForm = this.formBuilder.group({
    projectId: ['', Validators.required],
    inviteEmail:['',Validators.required]     
});

  let loginModel :LoginModel=this.loginstateService.getLoginState();
  this.userid=loginModel.id;
  this.userName=loginModel.username;
  this.orgId=loginModel.orgid;

  this.setActiveProjectData(this.userid);
  
  }

  get formcontrol() { return this.searchForm.controls; }
  get formfield() { return this.inviteForm.controls; }

  async setActiveProjectData(userid:number) {
     const response = await this.projectService.getActiveProject(userid);
    this.activeProjectData = response.data;
  }


  toggleInviteModal() {
    this.isHide = !this.isHide;
  }

  async onSearch(){
    this.submitted = true;

    if (this.searchForm.invalid) {
        return;
    }
    this.projectId=this.formcontrol.project.value;
    this.developerData=await this.userService.getdeveloperByProjectId(this.projectId);
    
  }


  async onRemove(useremail:string,projectid:number){
    this.userData=await this.userService.getprofileByEmail(useremail);
    
    const user:UserModel={
      id:this.userData[0].id,
      username:this.userData[0].username,
      useremail:this.userData[0].useremail,
      userpassword:this.userData[0].userpassword,
      userrole:this.userData[0].userrole,
      orgid:null
    }
  
   

    this.UserId=await this.userService.getprofileIdByEmail(useremail);
    this.userId=this.UserId[0];
    await this.projectService.removeUserProject(this.userId);
    await this.userService.updateUser(user);
    await this.userService.removeDeveloperByProjectId(useremail,projectid);
    window.location.reload();

  }

  async onInvite(){
    this.submit = true;

    if (this.inviteForm.invalid) {
        return;
    }

    this.projectName=await this.projectService.getProjectNameById(this.formfield.projectId.value);
    this.isUser=await this.userService.getprofileIdByEmail(this.formfield.inviteEmail.value);

    const invite:InviteModel={
      recipient: this.formfield.inviteEmail.value,
    sender:this.userName,
    project:null,
    url:null
    }

    const invitedeveloper:InviteDeveloperModel={
      id:0,
      useremail:this.formfield.inviteEmail.value,
      projectid:0,
      isaccept:0
    }
    
   if(this.isUser[0]===undefined){
    invitedeveloper.projectid=this.formfield.projectId.value;
    let response=await this.userService.addInviteDeveloper(invitedeveloper);
    if(response.code===201){
      invite.project=this.projectName[0];
      invite.url="http://localhost:4200/signup?project="+this.formfield.projectId.value+"&org="+this.orgId
      this.inviteForm.reset();
      await this.inviteService.sendInvitation(invite);
      this.popupService.setSuccessMessage('Successfully Developer Added !!');
      this.popupService.activateVisibleState();
    }
   }else{
    invitedeveloper.projectid=this.formfield.projectId.value;
    let response=await this.userService.addInviteDeveloper(invitedeveloper);
    if(response.code===201){
      invite.project=this.projectName[0];
      invite.url="http://localhost:4200/login?project="+this.formfield.projectId.value+"&org="+this.orgId
      this.inviteForm.reset();
      await this.inviteService.sendInvitation(invite);
      this.popupService.setSuccessMessage('Successfully developer Added !!');
      this.popupService.activateVisibleState();
    }
   }
    






  }
 
}

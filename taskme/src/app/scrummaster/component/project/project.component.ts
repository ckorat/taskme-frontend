import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PopupService } from '../../../common/services/popup/popup.service';
import { ProjectService } from '../../services/project/project.service';
import { ProjectModel } from '../../model/project/project-model';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { LoginModel } from 'src/app/common/model/login/login-model';
import { LoginstateService } from '../../../common/services/login_state/loginstate.service';
import { async } from '@angular/core/testing';
import { UserProjectModel } from 'src/app/common/model/userproject/userproject-model';
@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit { 

  isHide: boolean;
  projectForm: FormGroup;
  submitted = false;
  activeProjectData: [];
  inactiveProjectData: [];
  response: any;
  compDate:string;
  orgId:number;
  userid:number;
  projectId: any;
  userResponse: any;



  constructor(private formBuilder: FormBuilder, private projectService: ProjectService, private popupService: PopupService, private datePipe: DatePipe,
    private route:Router,private loginstateService:LoginstateService) { }

  async ngOnInit() {
    this.isHide = true;
    this.projectForm = this.formBuilder.group({
      projectname: ['', Validators.required],
      projectdescription: ['', [Validators.required,Validators.maxLength(50)]],
      projectstartdate: ['', Validators.required]
    });
   

    let loginModel :LoginModel=this.loginstateService.getLoginState();
    this.userid=loginModel.id;
    this.orgId=loginModel.orgid;

    this.setActiveProjectData(this.userid);
    this.setInactiveProjectData(this.userid);

     
    
  }

  viewModal() { this.isHide = !this.isHide; }
  closeModal() { this.isHide = !this.isHide; }

  get formcontrol() { return this.projectForm.controls; }

  async setActiveProjectData(userid:number) {
     const response = await this.projectService.getActiveProject(userid);
    this.activeProjectData = response.data;
  }

  async setInactiveProjectData(userid:number) {
    const response = await this.projectService.getInactiveProject(userid);
    this.inactiveProjectData = response.data;
  }

  async onSubmit() {
    this.submitted = true;

    if (this.projectForm.invalid) {
      return;
    }

    const project: ProjectModel = {
      id: null,
      orgId: this.orgId,
      projectName: this.formcontrol.projectname.value,
      projectDescription: this.formcontrol.projectdescription.value,
      createOn: this.formcontrol.projectstartdate.value,
      completeOn: null
    };

    const userproject: UserProjectModel = {
      id: null,
      projectId:null,
      userId:this.userid
    };

    this.response = await this.projectService.addNewProject(project);
    if (this.response.code === 201) {
      this.projectId=await this.projectService.getProjectIdByName(this.formcontrol.projectname.value);
      userproject.projectId = this.projectId[0];
      this.userResponse= await this.projectService.addUserProject(userproject);
      if(this.userResponse.code===201){
        this.setActiveProjectData(this.userid);
      this.projectForm.reset();
      this.submitted = false;
      this.popupService.setSuccessMessage('Successfully Project Added !!');
      this.popupService.activateVisibleState();
      }else{
       this.popupService.setErrorMessage(this.userResponse.message);
       this.popupService.activateVisibleState();
      }
    } else {
      this.popupService.setErrorMessage(this.response.message);
      this.popupService.activateVisibleState();
    }
  }

  async onUpdate(project:ProjectModel){
    this.response=await this.projectService.updateProject({
      id: project.id,
      orgId: project.orgId,
      projectName: project.projectName,
      projectDescription: project.projectDescription,
      createOn: project.createOn,
      completeOn:this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    });
    if (this.response.code === 200) {
      this.setInactiveProjectData(this.userid);
      this.setActiveProjectData(this.userid);
      this.popupService.setSuccessMessage('Successfully Project Closed !!');
      this.popupService.activateVisibleState();
    }else
    {
      this.popupService.setErrorMessage('Something Went Wrong..');
      this.popupService.activateVisibleState();
    }
  }
  onView(project:ProjectModel){
    const projectid=project.id;
    this.route.navigate(['/app/sprintlist'], { queryParams: { project: projectid }});
  }
}

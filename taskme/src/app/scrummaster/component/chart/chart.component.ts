import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  public pieChartLabels: string[] = ['In Progress', 'Complete', 'Test', 'ETC', 'SIT'];
  public pieChartData: number[] = [40, 20, 20 , 10, 10];
  public pieChartType = 'pie';
  constructor() { }
  ngOnInit(): void {}
}

import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ProjectComponent } from './component/project/project.component';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationComponent } from '../common/component/application/application.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManagedeveloperComponent } from './component/managedeveloper/managedeveloper.component';
import { ChartsModule } from 'ng2-charts';
import { HttpClientModule} from '@angular/common/http';
import { ChartComponent } from './component/chart/chart.component';

const routes: Routes = [
  { path: 'app', component: ApplicationComponent, children: [
    { path: '', pathMatch: 'full', redirectTo: 'projects' },
    { path: 'projects', component: ProjectComponent},
    { path: 'managedeveloper', component: ManagedeveloperComponent},
  ]}
];
@NgModule({
  declarations: [ProjectComponent, ManagedeveloperComponent,ChartComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    RouterModule,
    ChartsModule,
    HttpClientModule
  ],
  providers: [DatePipe],

})
export class ScrummasterModule { }

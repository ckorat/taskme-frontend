import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InviteModel } from '../../model/invite/invite-model';

@Injectable({
  providedIn: 'root'
})
export class InviteService {
  urlPath = 'http://localhost:8081/email-service/email';
  constructor(private http: HttpClient) { }

  async sendInvitation(data:InviteModel) : Promise<any>{
    return await this.http.post(this.urlPath+'/invitation', data).toPromise();
  }
}

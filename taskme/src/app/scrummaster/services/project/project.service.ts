import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProjectModel } from '../../model/project/project-model';
import { UserProjectModel } from 'src/app/common/model/userproject/userproject-model';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  urlPath = 'http://localhost:8081/issue-service/projects';
  constructor(private http: HttpClient) { }

  async getProject(): Promise<any> {
    return  await this.http.get(this.urlPath + '/').toPromise();
}

  async getActiveProject(id:number): Promise<any> {
      return  await this.http.get(this.urlPath + '/active/'+id).toPromise();
  }

  async getInactiveProject(id:number): Promise<any> {
      return await this.http.get(this.urlPath + '/inactive/'+id).toPromise();
  }

  async addNewProject(data:ProjectModel) : Promise<any>{
      return await this.http.post(this.urlPath+'/', data).toPromise();
  }

  async updateProject(updatedProject: ProjectModel): Promise<any> {
      return await this.http.put(this.urlPath+'/', updatedProject).toPromise();
  }

  async getProjectNameById(Id: number): Promise<any> {
    const projects = await this.http.get(this.urlPath+'/'+Id).toPromise();
    const project = projects["data"];
    return project.map(data => data.projectName);
  }
  
  async addUserProject(data:UserProjectModel):Promise<any>{
    return await this.http.post(this.urlPath+'/user', data).toPromise();
  }
  async updateUserProject(data:UserProjectModel):Promise<any>{
    return await this.http.put(this.urlPath+'/user/',data).toPromise();
  }
  async removeUserProject(UserId:number):Promise<any>{
    return await this.http.delete(this.urlPath+'/user/'+UserId).toPromise();
  }

  async getProjectByuserId(Id: number): Promise<any> {
    const userprojects = await this.http.get(this.urlPath+'/user/'+Id).toPromise();
    const userproject = userprojects["data"];
    return userproject.map(data => data.projectId);
  }

  async getProjectIdByName(name:string):Promise<any>{
    const projectList=await this.http.get(this.urlPath + '/').toPromise();
    const projects=projectList["data"];
    const project=projects.filter(data=>data.projectName === name);
    return project.map(data=>data.id);
  }
}

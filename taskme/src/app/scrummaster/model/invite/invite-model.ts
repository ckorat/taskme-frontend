export class InviteModel{
    recipient: string;
    sender:string;
    project:string;
    url:string;
}
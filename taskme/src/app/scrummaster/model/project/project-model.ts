export class ProjectModel {
    id: number;
    orgId: number;
    projectName: string;
    projectDescription: string;
    createOn: string;
    completeOn: string;
}
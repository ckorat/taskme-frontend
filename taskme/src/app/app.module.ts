import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { CreateMeetingComponent } from './common/component/create-meeting/create-meeting.component';
import { ScrummasterModule } from './scrummaster/scrummaster.module';
import { DeveloperModule } from './developer/developer.module';
import {commonModule} from './common/common.module';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { TokenService } from './common/services/token/token.service';
import { PopupComponent } from './common/component/popup/popup.component';
import { ChartsModule } from 'ng2-charts';
import { AuthGuardService } from './auth-guard.service';
import { LoginstateService } from './common/services/login_state/loginstate.service';

@NgModule({
  declarations: [
    AppComponent, PopupComponent,
  ],
  entryComponents: [CreateMeetingComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FullCalendarModule,
    BrowserAnimationsModule,
    MatDialogModule,
    FormsModule,
    CommonModule,
    ScrummasterModule,
    DeveloperModule,
    commonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ChartsModule
  ],
  providers: [DatePipe,AuthGuardService,LoginstateService, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }

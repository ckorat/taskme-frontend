import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { LoginstateService } from './common/services/login_state/loginstate.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public loginStateService: LoginstateService, public router: Router) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const roles = route.data.roles as Array<string>;
    if (!this.loginStateService.isLoginStateValidOfUser(roles[0])) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }

  }
}

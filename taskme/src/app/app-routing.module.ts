import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './common/component/index/index.component';
import { LoginComponent } from './common/component/login/login.component';
import { SignupComponent } from './common/component/signup/signup.component';
import { MeetingComponent } from './common/component/meeting/meeting.component';
import { PagenotfoundComponent } from './common/component/pagenotfound/pagenotfound.component';
import { ApplicationComponent } from './common/component/application/application.component';
import { SprintlistComponent } from './common/component/sprintlist/sprintlist.component';
import { NotificationComponent } from './common/component/notification/notification.component';
import { ProjectComponent } from './scrummaster/component/project/project.component';
import { SprintboardComponent } from './common/component/sprintboard/sprintboard.component';
import { MytaskComponent } from './developer/component/mytask/mytask.component';
import { TaskdetailComponent } from './common/component/taskdetail/taskdetail.component';
import { ManagedeveloperComponent } from './scrummaster/component/managedeveloper/managedeveloper.component';
import { SettingComponent } from './common/component/setting/setting.component';
import { ContactusComponent } from './common/component/contactus/contactus.component';
import { ChartComponent } from './scrummaster/component/chart/chart.component';

import { AuthGuardService } from './auth-guard.service';
import { ForgetpasswordComponent } from './common/component/forgetpassword/forgetpassword.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'index' },
  { path: 'index', component: IndexComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'contactus', component: ContactusComponent},
  { path: 'forgetpassword', component: ForgetpasswordComponent},
  { path: 'app', component: ApplicationComponent,children: [
    { path: '', pathMatch: 'full', redirectTo: 'projects' },
    { path: 'projects', component: ProjectComponent, canActivate: [AuthGuardService], data: { roles: ['SCRUM'] }},
    { path: 'meeting', component: MeetingComponent},
    { path: 'sprintlist', component: SprintlistComponent},
    { path: 'sprintboard', component: SprintboardComponent},
    { path: 'taskdetail', component: TaskdetailComponent},
    { path: 'notification', component: NotificationComponent},
    { path: 'chart', component: ChartComponent,canActivate: [AuthGuardService], data: { roles: ['SCRUM'] }},
    { path: 'mytask', component: MytaskComponent, canActivate: [AuthGuardService], data: { roles: ['DEV'] }},
    { path: 'managedeveloper', component: ManagedeveloperComponent, canActivate: [AuthGuardService], data: { roles: ['SCRUM'] }},
    { path: 'setting', component: SettingComponent},
  ]},
  { path: '**', component: PagenotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
